package personal.listviewanddynamiclistview;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Random;

public class SimpleList extends AppCompatActivity {
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list);
        this.initComponents();
    }



    private void initComponents() {
        this.listView = (ListView) this.findViewById(R.id.simple_list_view);
        String[] listItems = this.generateRandomNumbers();
        ArrayAdapter arrayAdapter = new ArrayAdapter(this.getApplicationContext(),R.layout.row,listItems);
        this.listView.setAdapter(arrayAdapter);
    }

    private String[] generateRandomNumbers() {
        String[] result = new String[10];
        for (int i=0; i<result.length; i++) {
            result[i] = String.valueOf(new Random().nextInt(Short.MAX_VALUE));
        }
        return result;
    }
}
