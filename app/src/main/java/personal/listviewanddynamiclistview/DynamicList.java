package personal.listviewanddynamiclistview;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class DynamicList extends AppCompatActivity {
    private EditText editTextIn;
    private Button buttonIn;
    private ListView listView;
    ArrayList<String> itemsList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_list);
        this.initComponents();
    }


    private void initComponents() {
        this.editTextIn = (EditText) this.findViewById(R.id.edit_text_in);
        this.buttonIn = (Button) this.findViewById(R.id.button_in);
        this.listView = (ListView) this.findViewById(R.id.dynamic_list_view);

        this.buttonIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = DynamicList.this.editTextIn.getText().toString();
                DynamicList.this.editTextIn.setText("");
                DynamicList.this.itemsList.add(text);
                ArrayAdapter arrayAdapter = new ArrayAdapter(DynamicList.this.getApplicationContext(),R.layout.row,DynamicList.this.itemsList);
                DynamicList.this.listView.setAdapter(arrayAdapter);
            }
        });

    }
}
