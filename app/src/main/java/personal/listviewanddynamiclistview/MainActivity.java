package personal.listviewanddynamiclistview;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button buttonSipmle;
    private Button buttonDynamic;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initComponents();
    }



    private void initComponents() {
        this.buttonSipmle = (Button) this.findViewById(R.id.button_simple_list_view);
        this.buttonDynamic = (Button) this.findViewById(R.id.button_dynamic_list_view);

        this.buttonSipmle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.intent = new Intent(MainActivity.this,SimpleList.class);
                MainActivity.this.startActivity(MainActivity.this.intent);
            }
        });

        this.buttonDynamic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.intent = new Intent(MainActivity.this,DynamicList.class);
                MainActivity.this.startActivity(MainActivity.this.intent);
            }
        });

    }
}
